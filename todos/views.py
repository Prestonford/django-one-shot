from django.shortcuts import render, get_object_or_404
from todos.models import TodoList


# Create your views here.
def todo_list(request):
    items = TodoList.objects.all()
    context = {
        "todo_list": items,
    }
    return render(request, "todos/list.html", context)


def todo_detail(request):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": list,
    }
    return render(request, "list/detail.html", context)
